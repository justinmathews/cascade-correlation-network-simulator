# Bibliography

<p style="padding-left: 2em; text-indent: -2em;">Ashby, F., & Valentin, V. (2004). Computational cognitive neuroscience: Building and testing biologically plausible computational models of neuroscience, neuroimaging, and behavioral data. <a href="https://www.researchgate.net/publication/237414518_Computational_Cognitive_Neuroscience_Building_and_Testing_Biologically_Plausible_Computational_Models_of_Neuroscience_Neuroimaging_and_Behavioral_Data">https://www.researchgate.net/publication/237414518_Computational_Cognitive_Neuroscience_Building_and_Testing_Biologically_Plausible_Computational_Models_of_Neuroscience_Neuroimaging_and_Behavioral_Data</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Balazs, G. (2009). Cascade-correlation neural networks: A survey. <a href="https://papersdb.cs.ualberta.ca/~papersdb/uploaded_files/1033/additional_ccnnsurvey2009.pdf">https://papersdb.cs.ualberta.ca/~papersdb/uploaded_files/1033/additional_ccnnsurvey2009.pdf</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Eltokhi, A., Janmaat, I. E., Genedi, M., Haarman, B. C. M., & Sommer, I. E. C. (2020). Dysregulation of synaptic pruning as a possible link between intestinal microbiota dysbiosis and neuropsychiatric disorders. Journal of Neuroscience Research, 98(7), 1335–1369. <a href="https://doi.org/10.1002/jnr.24616">https://doi.org/10.1002/jnr.24616</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Faust, T., Gunner, G., & Schafer, D. P. (2021). Mechanisms governing activity-dependent synaptic pruning in the mammalian CNS. Nature Reviews. Neuroscience, 22(11), 657–673. <a href="https://doi.org/10.1038/s41583-021-00507-y">https://doi.org/10.1038/s41583-021-00507-y</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Hansel, C. (2019). Deregulation of synaptic plasticity in autism. Neuroscience Letters, 688(Complete), 58–61. <a href="https://doi.org/10.1016/j.neulet.2018.02.003">https://doi.org/10.1016/j.neulet.2018.02.003</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Hiratani, N., & Fukai, T. (2018). Redundancy in synaptic connections enables neurons to learn optimally. Proceedings of the National Academy of Sciences, 115(29), E6871–E6879. <a href="https://doi.org/10.1073/pnas.1803274115">https://doi.org/10.1073/pnas.1803274115</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Huttenlocher, P. R. (1984). Synapse elimination and plasticity in developing human cerebral cortex. American Journal of Mental Deficiency, 88(5), 488–496.</p>

<p style="padding-left: 2em; text-indent: -2em;">Huttenlocher, P. R., de Courten, C., Garey, L. J., & Van der Loos, H. (1982). Synaptogenesis in human visual cortex—Evidence for synapse elimination during normal development. Neuroscience Letters, 33(3), 247–252. <a href="https://doi.org/10.1016/0304-3940(82)90379-2">https://doi.org/10.1016/0304-3940(82)90379-2</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Kuzawa, C. W., Chugani, H. T., Grossman, L. I., Lipovich, L., Muzik, O., Hof, P. R., Wildman, D. E., Sherwood, C. C., Leonard, W. R., & Lange, N. (2014). Metabolic costs and evolutionary implications of human brain development. Proceedings of the National Academy of Sciences, 111(36), 13010–13015. <a href="https://doi.org/10.1073/pnas.1323099111">https://doi.org/10.1073/pnas.1323099111</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Lang, K. J., & Witbrock, M. J. (1988). Learning to tell two spirals apart. <a href="https://www.researchgate.net/publication/269337640_Learning_to_Tell_Two_Spirals_Apart">https://www.researchgate.net/publication/269337640_Learning_to_Tell_Two_Spirals_Apart</a></p>

<p style="padding-left: 2em; text-indent: -2em;">LeCun, Y., Denker, J., & Solla, S. (1989). Optimal brain damage. Advances in Neural Information Processing Systems, 2. <a href="https://proceedings.neurips.cc/paper/1989/hash/6c9882bbac1c7093bd25041881277658-Abstract.html">https://proceedings.neurips.cc/paper/1989/hash/6c9882bbac1c7093bd25041881277658-Abstract.html</a></p>


<p style="padding-left: 2em; text-indent: -2em;">Low, L. K., & Cheng, H.-J. (2006). Axon pruning: An essential step underlying the developmental plasticity of neuronal connections. Philosophical Transactions of the Royal Society B: Biological Sciences, 361(1473), 1531–1544. <a href="https://doi.org/10.1098/rstb.2006.1883">https://doi.org/10.1098/rstb.2006.1883</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Padamsey, Z., & Rochefort, N. L. (2023). Paying the brain’s energy bill. Current Opinion in Neurobiology, 78, 102668. <a href="https://doi.org/10.1016/j.conb.2022.102668">https://doi.org/10.1016/j.conb.2022.102668</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Paolicelli, R. C., Bolasco, G., Pagani, F., Maggi, L., Scianni, M., Panzanelli, P., Giustetto, M., Ferreira, T. A., Guiducci, E., Dumas, L., Ragozzino, D., & Gross, C. T. (2011). Synaptic pruning by microglia is necessary for normal brain development. Science, 333(6048), 1456–1458. <a href="https://doi.org/10.1126/science.1202529">https://doi.org/10.1126/science.1202529</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Petanjek, Z., Judaš, M., Šimić, G., Rašin, M. R., Uylings, H. B. M., Rakic, P., & Kostović, I. (2011). Extraordinary neoteny of synaptic spines in the human prefrontal cortex. Proceedings of the National Academy of Sciences, 108(32), 13281–13286. <a href="https://doi.org/10.1073/pnas.1105108108">https://doi.org/10.1073/pnas.1105108108</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Prechelt, L. (1997). Investigation of the CasCor Family of Learning Algorithms. Neural Networks, 10(5), 885–896. <a href="https://doi.org/10.1016/S0893-6080(96)00115-3">https://doi.org/10.1016/S0893-6080(96)00115-3</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Reed, R. (1993). Pruning algorithms-a survey. IEEE Transactions on Neural Networks, 4(5), 740–747. <a href="https://doi.org/10.1109/72.248452">https://doi.org/10.1109/72.248452</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Sakai, J. (2020). How synaptic pruning shapes neural wiring during development and, possibly, in disease. Proceedings of the National Academy of Sciences of the United States of America, 117(28), 16096–16099. <a href="https://doi.org/10.1073/pnas.2010281117">https://doi.org/10.1073/pnas.2010281117</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Sekar, A., Bialas, A. R., de Rivera, H., Davis, A., Hammond, T. R., Kamitaki, N., Tooley, K., Presumey, J., Baum, M., Van Doren, V., Genovese, G., Rose, S. A., Handsaker, R. E., Daly, M. J., Carroll, M. C., Stevens, B., & McCarroll, S. A. (2016). Schizophrenia risk from complex variation of complement component 4. Nature, 530(7589), 177–183. <a href="https://doi.org/10.1038/nature16549">https://doi.org/10.1038/nature16549</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Siekmeier, P. J., & Hoffman, R. E. (2002). Enhanced semantic priming in schizophrenia: A computer model based on excessive pruning of local connections in association cortex. The British Journal of Psychiatry, 180(4), 345–350. <a href="https://doi.org/10.1192/bjp.180.4.345">https://doi.org/10.1192/bjp.180.4.345</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Tang, G., Gudsnuk, K., Kuo, S.-H., Cotrina, M. L., Rosoklija, G., Sosunov, A., Sonders, M. S., Kanter, E., Castagna, C., Yamamoto, A., Yue, Z., Arancio, O., Peterson, B. S., Champagne, F., Dwork, A. J., Goldman, J., & Sulzer, D. (2014). Loss of mTOR-dependent aacroautophagy causes autistic-like synaptic pruning deficits. Neuron, 83(5), 1131–1143. <a href="https://doi.org/10.1016/j.neuron.2014.07.040">https://doi.org/10.1016/j.neuron.2014.07.040</a></p>

<p style="padding-left: 2em; text-indent: -2em;">Thivierge, J. P., Rivest, F., & Shultz, T. R. (2003). A dual-phase technique for pruning constructive networks. Proceedings of the International Joint Conference on Neural Networks, 2003., 1, 559–564 vol.1. <a href="https://doi.org/10.1109/IJCNN.2003.1223407">https://doi.org/10.1109/IJCNN.2003.1223407</a></p>