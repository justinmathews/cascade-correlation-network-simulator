from unittest import TestCase
from pycascor.activation_functions import AsymmetricSigmoid, Sigmoid, Gaussian, Linear


class TestSigmoid(TestCase):
    def test_activation_sum_less_negative_15(self):
        actual = Sigmoid.activation(-15.1)
        self.assertAlmostEqual(-0.5, actual)

    def test_activation_sum_greater_than_15(self):
        actual = Sigmoid.activation(15.1)
        self.assertAlmostEqual(0.5, actual)

    def test_activation_sum_negative_between_0_15(self):
        actual = Sigmoid.activation(-14.9)
        self.assertGreaterEqual(actual, -0.5)
        self.assertLess(actual, 0)

        actual = Sigmoid.activation(-0.0001)
        self.assertGreaterEqual(actual, -0.5)
        self.assertLess(actual, 0)

    def test_activation_sum_positive_between_0_15(self):
        actual = Sigmoid.activation(14.9)
        self.assertLessEqual(actual, 0.5)
        self.assertGreater(actual, 0)

        actual = Sigmoid.activation(0.0001)
        self.assertLessEqual(actual, 0.5)
        self.assertGreater(actual, 0)

    def test_activation_prime(self):
        actual = Sigmoid.activation_prime(0.0001)
        self.assertAlmostEqual(0.24999999, actual)

    def test_output_sum_less_negative_15(self):
        actual = Sigmoid.output(-15.1)
        self.assertAlmostEqual(-0.5, actual)

    def test_output_sum_greater_than_15(self):
        actual = Sigmoid.output(15.1)
        self.assertAlmostEqual(0.5, actual)

    def test_output_sum_negative_between_0_15(self):
        actual = Sigmoid.output(-14.9)
        self.assertGreaterEqual(actual, -0.5)
        self.assertLess(actual, 0)

        actual = Sigmoid.output(-0.0001)
        self.assertGreaterEqual(actual, -0.5)
        self.assertLess(actual, 0)

    def test_output_sum_positive_between_0_15(self):
        actual = Sigmoid.output(14.9)
        self.assertLessEqual(actual, 0.5)
        self.assertGreater(actual, 0)

        actual = Sigmoid.output(0.0001)
        self.assertLessEqual(actual, 0.5)
        self.assertGreater(actual, 0)

    def test_output_prime_positive_output(self):
        actual = Sigmoid.output_prime(0.5)
        self.assertAlmostEqual(0.1, actual)

        actual = Sigmoid.output_prime(0.4)
        self.assertAlmostEqual(0.19, actual)

    def test_output_prime_negative_output(self):
        actual = Sigmoid.output_prime(-0.5)
        self.assertAlmostEqual(0.1, actual)

        actual = Sigmoid.output_prime(-0.4)
        self.assertAlmostEqual(0.19, actual)

    def test_output_prime_zero_output(self):
        actual = Sigmoid.output_prime(0.0)
        self.assertAlmostEqual(0.35, actual)


class TestAsymmetricSigmoid(TestCase):
    def test_activation_sum_less_negative_15(self):
        actual = AsymmetricSigmoid.activation(-15.1)
        self.assertAlmostEqual(0, actual)

    def test_activation_sum_greater_than_15(self):
        actual = AsymmetricSigmoid.activation(15.1)
        self.assertAlmostEqual(1, actual)

    def test_activation_sum_negative_between_0_15(self):
        actual = AsymmetricSigmoid.activation(-14.9)
        self.assertGreaterEqual(actual, 0)
        self.assertLess(actual, 0.5)

        actual = AsymmetricSigmoid.activation(-0.0001)
        self.assertGreaterEqual(actual, 0)
        self.assertLess(actual, 0.5)

    def test_activation_sum_positive_between_0_15(self):
        actual = AsymmetricSigmoid.activation(14.9)
        self.assertLessEqual(actual, 1)
        self.assertGreater(actual, 0.5)

        actual = AsymmetricSigmoid.activation(0.0001)
        self.assertLessEqual(actual, 1)
        self.assertGreater(actual, 0.5)

    def test_activation_prime_one(self):
        actual = AsymmetricSigmoid.activation_prime(1.0)
        self.assertEqual(actual, 0)

    def test_activation_prime_zero(self):
        actual = AsymmetricSigmoid.activation_prime(0.0)
        self.assertEqual(actual, 0)

    def test_activation_prime_positive(self):
        actual = AsymmetricSigmoid.activation_prime(0.5)
        self.assertEqual(actual, 0.25)


class TestGaussian(TestCase):
    def test_activation_sum_less_negative_75(self):
        actual = Gaussian.activation(-75.1)
        self.assertAlmostEqual(0, actual)

    def test_activation_sum_greater_than_negative_75(self):
        actual = Gaussian.activation(1.1)
        self.assertAlmostEqual(0.5460744266397094, actual)

    def test_activation_prime_value_zero(self):
        actual = Gaussian.activation_prime(0.0, 1.0)
        self.assertAlmostEqual(actual, 0.0)

        actual = Gaussian.activation_prime(0.0, -1.0)
        self.assertAlmostEqual(actual, 0.0)

    def test_activation_prime_value_positive(self):
        actual = Gaussian.activation_prime(1.0, 1.0)
        self.assertEqual(actual, -1.0)

        actual = Gaussian.activation_prime(2.0, 1.0)
        self.assertEqual(actual, -2.0)

    def test_activation_prime_negative_sum(self):
        actual = Gaussian.activation_prime(1.0, -1.0)
        self.assertEqual(actual, 1.0)

        actual = Gaussian.activation_prime(0.5, -2.0)
        self.assertEqual(actual, 1.0)


class TestLinear(TestCase):
    def test_output(self):
        actual = Linear.output(5)
        self.assertEqual(5, actual)

        actual = Linear.output(-5)
        self.assertEqual(-5, actual)

    def test_output_prime(self):
        actual = Linear.output_prime(-5)
        self.assertEqual(1.0, actual)

        actual = Linear.output_prime(500)
        self.assertEqual(1.0, actual)

        actual = Linear.output_prime(0.0)
        self.assertEqual(1.0, actual)
