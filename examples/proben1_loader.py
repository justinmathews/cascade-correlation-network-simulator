from collections import defaultdict
from typing import List, Dict, Any


def load_problem(filename) -> tuple[Dict[str, List], Dict[str, List], Dict[str, List], Dict[str, Any]]:

    with open(filename) as training_file:
        data_file = training_file.readlines()

        header = data_file[:7]
        training_data = data_file[7:]

        metadata = dict([k.strip().split('=') for k in header])
        metadata: Dict[str, Any] = {k: int(v) for (k, v) in metadata.items()}

        print(metadata)

        training_data = [k.strip().split(' ') for k in training_data]

        parsed_training_data: Dict[str, List] = defaultdict(list)
        parsed_validation_data: Dict[str, List] = defaultdict(list)
        parsed_test_data: Dict[str, List] = defaultdict(list)
        count = 0

        for row in training_data:
            count += 1
            inputs = []
            outputs = []

            for k in range(0, metadata['bool_in']):
                inputs.append(int(row[k]))

            for k in range(metadata['bool_in'], metadata['bool_in'] + metadata['real_in']):
                inputs.append(float(row[k]))

            for k in range(metadata['bool_in'] + metadata['real_in'], metadata['bool_in'] + metadata['real_in'] + metadata['bool_out']):
                outputs.append(int(row[k]))

            for k in range(metadata['bool_in'] + metadata['real_in'] + metadata['bool_out'], metadata['bool_in'] + metadata['real_in'] + metadata['bool_out'] + metadata['real_out']):
                outputs.append(float(row[k]))

            if count <= metadata["training_examples"]:
                parsed_training_data["inputs"].append(inputs)
                parsed_training_data["outputs"].append(outputs)
            elif count <= metadata["training_examples"] + metadata["validation_examples"]:
                parsed_validation_data["inputs"].append(inputs)
                parsed_validation_data["outputs"].append(outputs)
            elif count <= metadata["training_examples"] + metadata["validation_examples"] + metadata["test_examples"]:
                parsed_test_data["inputs"].append(inputs)
                parsed_test_data["outputs"].append(outputs)

    print(filename, len(parsed_training_data["inputs"]), len(parsed_training_data["outputs"]))
    print(filename, len(parsed_validation_data["inputs"]), len(parsed_validation_data["outputs"]))
    print(filename, len(parsed_test_data["inputs"]), len(parsed_test_data["outputs"]))
    return parsed_training_data, parsed_validation_data, parsed_test_data, metadata










