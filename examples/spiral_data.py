from typing import List, Dict
from random import shuffle

import numpy as np


def build_two_spirals(n=97) -> tuple[List[List[float]], List[List[float]]]:
    inputs = []
    outputs = []

    # Populate ti and to arrays
    for i in range(n):
        angle = i * (np.pi / 16.0)
        radius = 6.5 * (104.0 - i) / 104.0
        x = radius * np.sin(angle)
        y = radius * np.cos(angle)

        inputs.append([x, y])
        inputs.append([-x, -y])
        outputs.append([0.5, ])
        outputs.append([-0.5, ])

    # Reverse input and output arrays
    inputs.reverse()
    outputs.reverse()

    return inputs, outputs


def randomized_spirals(n=97) -> tuple[List[List[float]], List[List[float]]]:
    inputs, outputs = build_two_spirals(n)

    combined = list(zip(inputs, outputs))
    shuffle(combined)

    print(combined)
    shuffled_inputs: List[List[float]] = [x[0] for x in combined]
    shuffled_outputs: List[List[float]] = [x[1] for x in combined]

    return shuffled_inputs, shuffled_outputs


def partitioned_spirals(n=97) -> tuple[Dict[str, List], Dict[str, List], Dict[str, List]]:
    inputs, outputs = randomized_spirals(n)

    training_n = n
    validation_n = training_n // 2

    training_data = {
        "inputs": inputs[:training_n],
        "outputs": outputs[:training_n]
    }
    validation_data = {
        "inputs": inputs[:training_n],
        "outputs": outputs[:training_n]
    }
    testing_data = {
        "inputs": inputs[:training_n],
        "outputs": outputs[:training_n]
    }
    # validation_data = {
    #     "inputs": inputs[training_n:training_n + validation_n],
    #     "outputs": outputs[training_n:training_n + validation_n]
    # }
    # testing_data = {
    #     "inputs": inputs[training_n + validation_n:],
    #     "outputs": outputs[training_n + validation_n:]
    # }
    return training_data, validation_data, testing_data
